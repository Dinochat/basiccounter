#BasicCounter 

A lire pour l'application Basic Counter

Principe de l'application : 
On appuie sur un bouton + pour ajouter et sur le bouton - pour enlever. 
Le bouton RAZ sert à réinitialiser la valeur finale qu'on veut obtenir.


Niveau programmation : 
Il se découpe en deux partie : la forme et la classe.
Dans la forme se trouvent les évènements pour cliquer sur les boutons.
Dans la classe se trouvent les fonctions qui permettent d'incrémenter, de décrémenter et de réinitialiser le compteur.
On utilise les tests unitaires pour valider les fonctions. 

Diagramme UML : 
(\TP_progwind\basiccounter\BasicCounter\UML_BC.png)
Pour la classe BClasse de l'application