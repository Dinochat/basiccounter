﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BClasse;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestIncrement()
        {
            BCounterclass MyClass = new BCounterclass(1);
            Assert.AreEqual(1, MyClass.Increment());
        }

        public void TestDecrement()
        {
            BCounterclass MyClass = new BCounterclass(1);
            Assert.AreEqual(-1, MyClass.Decrement());
        }

        public void TestRAZ()
        {
            BCounterclass MyClass = new BCounterclass(0);
            Assert.AreEqual(0, MyClass.RAZ());
        }
    }
}
