﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BClasse
{
    public class BCounterclass
    {
        protected 
            int valeur; //Si veut rajouter classe fille, y aura pas de soucis pour utiliser les attributs

        public BCounterclass(int value)
        {
           valeur = value;
        }

        public  int Increment()
        {
            valeur = valeur + 1;

            return valeur;
        }

        public  int Decrement()
        {
            if (valeur == 0)
                valeur = 0;
            
            if(valeur != 0)
                valeur = valeur - 1;

            return valeur;
        }

        public int RAZ()
        {
            valeur = 0;
            return valeur;
        }
    }
}
