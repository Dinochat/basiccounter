﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BClasse;

namespace BasicCounter
{
    public partial class Form1 : Form
    {

        BCounterclass compteur;

        public Form1()
        {
            InitializeComponent();
            this.compteur = new BCounterclass(0); 
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ajouter_Click(object sender, EventArgs e) 
        {
            int valeur;
            valeur = this.compteur.Increment();
            label1.Text = String.Format(valeur.ToString());
           
        }

        private void retirer_Click(object sender, EventArgs e)
        {
            int valeur;
            valeur = this.compteur.Decrement();
            label1.Text = String.Format(valeur.ToString());
        }

        private void raz_Click(object sender, EventArgs e)
        {
            int valeur;
            valeur = this.compteur.RAZ();
            label1.Text = String.Format(valeur.ToString());
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
